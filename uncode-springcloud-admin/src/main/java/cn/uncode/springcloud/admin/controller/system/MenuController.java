package cn.uncode.springcloud.admin.controller.system;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.uncode.springcloud.admin.model.system.dto.DeskIconDTO;
import cn.uncode.springcloud.admin.service.system.DeskIconService;
import cn.uncode.springcloud.starter.web.result.P;
import cn.uncode.springcloud.starter.web.result.R;


@Controller
@RequestMapping("/menus")
public class MenuController {

	@Autowired
	DeskIconService deskIconService;
    

    //添加路由信息
    @RequestMapping(value = "/add" , method = RequestMethod.POST)
    @ResponseBody
    public  R<Boolean> add(@RequestBody DeskIconDTO canary){
    	long id = deskIconService.add(canary);
    	if(id > 0) {
    		return R.success();
    	}
        return R.failure();
    }

    @RequestMapping(value = "/update" , method = RequestMethod.POST)
    @ResponseBody 
    public R<Boolean> edit(@RequestBody DeskIconDTO canary){
    	int count = deskIconService.update(canary);
    	if(count > 0) {
    		return R.success();
    	}
        return R.failure();
    }

    //打开路由列表
    @RequestMapping(value = "/list" , method = RequestMethod.GET)
    @ResponseBody
    public R<List<DeskIconDTO>> list(@RequestParam int page, @RequestParam int limit){
    	P<List<DeskIconDTO>> pageRt = deskIconService.getPageList(page, limit);
        return R.success(pageRt).fill("count", pageRt.getRecordTotal());
    }

    @RequestMapping(value = "/delete" , method = RequestMethod.GET)
    @ResponseBody
    public  R<Boolean> delete(@RequestParam String ids){
    	if(StringUtils.isNotBlank(ids)) {
    		List<Long> idList = new ArrayList<>();
    		for(String id:ids.split(",")) {
    			if(StringUtils.isNotBlank(id)) {
    				idList.add(Long.valueOf(id));
    			}
    		}
    		int count = deskIconService.delete(idList);
        	if(count > 0) {
        		return R.success();
        	}
    	}
    	
        return R.failure();
    }
    
    @RequestMapping(value = "/updateOrder" , method = RequestMethod.POST)
    @ResponseBody 
    public R<Boolean> updateOrder(@RequestParam long id, @RequestParam int order){
    	DeskIconDTO route = new DeskIconDTO();
    	route.setId(id);
    	route.setOrder(order);
    	int count = deskIconService.update(route);
    	if(count > 0) {
    		return R.success();
    	}
        return R.failure();
    }
   
}
